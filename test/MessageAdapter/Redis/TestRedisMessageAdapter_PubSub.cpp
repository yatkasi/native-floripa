#include "catch2/catch.hpp"

#include "Poco/Event.h"

#include "MessageAdapter/Redis/RedisMessageAdapter.h"
#include "MessageAdapter/Redis/IRedisMessageHandler.h"

using namespace Poco::Redis;

Poco::Event testMessageArrived(true);

enum RedisMessageTypes {
    unknown,
    subscribe,
    psubscribe,
    unsubscribe,
    message
};

class TestRedisMessageHandlerImpl : public IRedisMessageHandler {

public:
    RedisMessageTypes resolveType(std::string input) {
        static const std::map<std::string, RedisMessageTypes> mapping {
            {"subscribe", subscribe},
            {"psubscribe", psubscribe},
            {"unsubscribe", unsubscribe},
            {"message", message},
        };
        auto itr = mapping.find(input);
        if( itr != mapping.end() ) {
            return itr->second;
        }
        return unknown;
    }

    RedisMessageTypes receivedType;
    BulkString receivedKey;
    BulkString receivedMessage;
    int messageCounter = 0;

    void onMessage(const void *pSender, RedisEventArgs &args) override {
        if (args.message().isNull()) FAIL("do not expect empty message");

        Type<Array>* arrayType = dynamic_cast<Type<Array>*>(args.message().get());
        if (arrayType == NULL) FAIL("expect message is an type array");

        Array &array = arrayType->value();
        receivedType = resolveType(array.get<BulkString>(0));
        switch (receivedType)
        {
            case unsubscribe:
            case subscribe:
            case psubscribe:
                {
                    REQUIRE(array.size() == 3);
                    receivedKey = array.get<BulkString>(1);
                    auto subscriptionCount = array.get<Poco::Int64>(2);
                    std::cout << receivedType << ":" << receivedKey << " " << subscriptionCount << std::endl;
                    break;
                }

            case message:
            {
                REQUIRE(array.size() == 3);
                receivedKey = array.get<BulkString>(1);
                receivedMessage = array.get<BulkString>(2);
                std::cout << receivedType << ":" << receivedKey << " " << receivedMessage << std::endl;
                testMessageArrived.set();
                break;
            }

            case unknown:
            default:
                {
                    auto stringValue = array.toString();
                    std::cerr << "unknown redis type, raw message " << stringValue << std::endl;
                    FAIL(stringValue);
                    break;
                }
        }
        messageCounter++;
    }

    void onError(const void *pSender, RedisEventArgs &args) override {
        FAIL("Do not expect onError is called, something is wrong - check your redis docker environment ");
    }

};

class ConnectAdapterRAII {
    RedisMessageAdapter& _adapter;
public:
    ConnectAdapterRAII(RedisMessageAdapter& adapter) : _adapter(adapter) { _adapter.connect(); }
    ~ConnectAdapterRAII() { _adapter.disconnect(); }
};

SCENARIO("Test Redis Pubsub", "[bdd][redis][pubsub]")
{
    auto testRedisMessageHandler = std::make_shared<TestRedisMessageHandlerImpl>();
    RedisMessageAdapter adapterUnderTest(testRedisMessageHandler);
    RedisMessageAdapter redisTestUtil(std::make_shared<NoopRedisMessageHandlerImpl>());

    WHEN("set new message") {
        THEN("receive message with key and value") {
            std::string intendedChannel = "TestChannel";
            std::string intendedMessage = "A1234567";

            ConnectAdapterRAII t(redisTestUtil);
            ConnectAdapterRAII a(adapterUnderTest);

            adapterUnderTest.subscribeMessages(intendedChannel);

            redisTestUtil.sendMessage(intendedChannel, intendedMessage);
            testMessageArrived.tryWait(1000);

            REQUIRE(!testRedisMessageHandler->receivedKey.isNull());
            REQUIRE(intendedChannel.compare(testRedisMessageHandler->receivedKey.value()) == 0);

            REQUIRE(!testRedisMessageHandler->receivedMessage.isNull());
            REQUIRE(intendedMessage.compare(testRedisMessageHandler->receivedMessage.value()) == 0);

            REQUIRE(message == testRedisMessageHandler->receivedType);

        }
    }

}

