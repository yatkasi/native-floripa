#include "catch2/catch.hpp"

#include "MessageAdapter/Redis/RedisMessageAdapter.h"
#include "MessageAdapter/Redis/IRedisMessageHandler.h"

SCENARIO("Test Redis Connection", "[bdd][redis][message_adapter]")
{
    auto msgHandler = std::make_shared<NoopRedisMessageHandlerImpl>();
    RedisMessageAdapter adapterUnderTest(msgHandler);

    WHEN("Connect & Disconnect") {
        THEN("Expects isConnected changes from false to true to false") {
            REQUIRE(!adapterUnderTest.isConnected());

            REQUIRE(adapterUnderTest.connect());
            REQUIRE(adapterUnderTest.isConnected());

            REQUIRE(adapterUnderTest.disconnect());
            REQUIRE(!adapterUnderTest.isConnected());
        }
    }

    WHEN("sendMessage Is Ok") {
        THEN("sendMessage returns true") {
            REQUIRE(adapterUnderTest.connect());
            REQUIRE(adapterUnderTest.sendMessage("SomeKey", "SomeMessage"));
            REQUIRE(adapterUnderTest.disconnect());
        }
    }

}