//
// Interface for MessageAdapter
// Created by antho on 11/16/2019.
//

#ifndef HELLO_IMESSAGEADAPTER_H
#define HELLO_IMESSAGEADAPTER_H

class IMessageAdapter {
public:
    virtual bool connect() = 0;
    virtual bool disconnect() = 0;
    virtual bool subscribeMessages(std::string topic) = 0;
    virtual bool sendMessage(const std::string &key, const std::string &value) = 0;
    virtual bool isConnected() = 0;
};

#endif //HELLO_IMESSAGEADAPTER_H
