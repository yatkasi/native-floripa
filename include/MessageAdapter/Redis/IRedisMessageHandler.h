//
// Created by antho on 11/17/2019.
//

#ifndef HELLO_IRedisMessageHandler_H
#define HELLO_IRedisMessageHandler_H

#include "Poco/Redis/RedisEventArgs.h"

using Poco::Redis::RedisEventArgs;

class IRedisMessageHandler
{
public:
    virtual void onMessage(const void* pSender, RedisEventArgs& args) = 0;
    virtual void onError(const void* pSender, RedisEventArgs& args) = 0;
};

class NoopRedisMessageHandlerImpl : public IRedisMessageHandler
{
public:
    void onMessage(const void* pSender, RedisEventArgs& args) {}
    void onError(const void* pSender, RedisEventArgs& args) {}
};

#endif //HELLO_IRedisMessageHandler_H
