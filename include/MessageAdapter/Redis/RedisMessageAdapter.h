//
// Created by antho on 11/16/2019.
//

#ifndef HELLO_REDISMESSAGEADAPTER_H
#define HELLO_REDISMESSAGEADAPTER_H

#include "MessageAdapter/IMessageAdapter.h"
#include "MessageAdapter/Redis/IRedisMessageHandler.h"
#include "MessageAdapter/Redis/RedisMessageAdapter.h"
#include "Poco/Redis/AsyncReader.h"
#include "Poco/Redis/Client.h"
#include "Poco/Logger.h"
using Poco::Logger;

class RedisMessageAdapter : public IMessageAdapter {
public:
    RedisMessageAdapter(std::shared_ptr<IRedisMessageHandler> redisMessageHandler);

    bool connect() override;
    bool disconnect() override;
    bool subscribeMessages(std::string topic) override;
    bool sendMessage(const std::string &key, const std::string &value) override;
    bool isConnected() override;

private:
    Logger& logger = Logger::get("RedisMessageAdapter");
    std::shared_ptr<IRedisMessageHandler> _redisMessageHandler;

    Poco::Redis::Client _client;
    Poco::Redis::AsyncReader _asyncReader;
};

#endif //HELLO_REDISMESSAGEADAPTER_H

