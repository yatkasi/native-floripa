[![pipeline status](https://gitlab.com/yatkasi/native-floripa/badges/master/pipeline.svg)](https://gitlab.com/yatkasi/native-floripa/commits/master)
[![coverage report](https://gitlab.com/yatkasi/native-floripa/badges/master/coverage.svg)](https://gitlab.com/yatkasi/native-floripa/commits/master)

# Native Floripa

## C++ Project example using Gitlab CI

#### Building
To build this Project
please refer to Wiki of this repository  https://gitlab.com/yatkasi/native-floripa/wikis/home

#### Requirements
- CMake
- gcc
- Conan
- Doxygen

#### LICENSE
[MIT](LICENSE)
