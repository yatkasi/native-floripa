//
// Created by antho on 11/16/2019.
//

#include <memory>
#include "Poco/Delegate.h"
#include "Poco/Redis/Redis.h"
#include "Poco/Redis/Array.h"

#include "MessageAdapter/Redis/RedisMessageAdapter.h"
#include "MessageAdapter/Redis/IRedisMessageHandler.h"

RedisMessageAdapter::RedisMessageAdapter(std::shared_ptr<IRedisMessageHandler> redisMessageHandler)     :
        _redisMessageHandler(redisMessageHandler),
        _asyncReader(_client)
{
}

bool RedisMessageAdapter::connect() {
    auto host = "redis";
    auto port = 6379;
    auto pwd = "";

    if (!_client.isConnected())
    {
        try
        {
            Poco::Timespan t(10, 0); // Connect within 10 seconds
            _client.connect(host, port, t);
        }
        catch (Poco::Exception& e)
        {
            logger.fatal("Failed to connect to redis");
            logger.log(e);
        }
    }
    return _client.isConnected();
}

bool RedisMessageAdapter::disconnect() {
    if (_client.isConnected()) {
        try
        {
            _client.disconnect();
        }
        catch (Poco::Exception& e)
        {
            logger.fatal("Failed to disconnect from redis");
            logger.log(e);
        }
    }
    return true;
}

bool RedisMessageAdapter::isConnected()
{
    if (!_client.isConnected()) return false;
    Poco::Redis::Array ping_command;
    ping_command.add("PING");
    std::string result = _client.execute<std::string>(ping_command);
    std::string expectedResult = "PONG";
    return (expectedResult.compare(result) == 0);
}

bool RedisMessageAdapter::subscribeMessages(std::string topic) {
    Poco::Redis::Array subscribe;
    subscribe.add("SUBSCRIBE").add(topic);
    _client.execute<void>(subscribe);
    _client.flush();

    // beware of memory leak - rawPointer is passed to asyncReader.
    _asyncReader.redisResponse += Poco::delegate(_redisMessageHandler.get(), &IRedisMessageHandler::onMessage);
    _asyncReader.redisException += Poco::delegate(_redisMessageHandler.get(), &IRedisMessageHandler::onError);
    _asyncReader.start();

    return true;
}

bool RedisMessageAdapter::sendMessage(const std::string &key, const std::string &value) {
    try
    {
        Poco::Redis::Array command;
        command.add("publish").add(key).add(value);
        auto anything = _client.sendCommand(command);
        return true;
    }
    catch (Poco::Redis::RedisException& e)
    {
        logger.log(e.message());
        return false;
    }
}